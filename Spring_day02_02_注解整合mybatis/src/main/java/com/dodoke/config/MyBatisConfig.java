package com.dodoke.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public class MyBatisConfig {



    @Bean
    public  SqlSessionFactoryBean getSqlSessionFactoryBean(@Autowired DruidDataSource dataSource){
        SqlSessionFactoryBean sqlSessionFactoryBean= new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setTypeAliasesPackage("com.dodoke.domain");
        return sqlSessionFactoryBean;
    }

    @Bean
    public  MapperScannerConfigurer getMapperScannerConfigurer(){
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.dodoke.dao");
        return  mapperScannerConfigurer;
    }
}
