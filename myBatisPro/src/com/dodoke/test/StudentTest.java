package com.dodoke.test;

import com.dodoke.bean.Student;
import com.dodoke.dao.StudentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

public class StudentTest {
    public static void main(String[] args) {
        try {


            InputStream is = Resources.getResourceAsStream("config/MyBatisConfig.xml");
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
            SqlSession sqlSession = sqlSessionFactory.openSession();

////      ?      StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
//            ClassesDao classesDao = sqlSession.getMapper(ClassesDao.class);
//            PageHelper.startPage(1,3);

//            List <Student> list = studentDao.selectAllOne();
//
//            for (Student stu : list) {
//                System.out.println(stu.getName());
//                System.out.println(stu.getCard().getNumber());
//            }

//            List <Classes> list = classesDao.selectAll();
////
//            for (Classes stu : list) {
//                System.out.println(stu.getName());
//               List <Student> studentList = stu.getStudents();
//               for (Student s:studentList){
//                   System.out.println(s.getName());
//               }
//            }


            StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
            Student student = new Student();
            student.setAge(10);
            student.setId(1);
            student.setName("新同学:"+10);
            mapper.insert(student);


//            Student student = new Student();
//            student.setAge(10);
//            student.setName("name:"+10);
//            studentDao.save(student);
//            Student student = new Student();
//
//            student.setId(1);
//            Student  s = studentDao.findByCondition(student);
//            System.out.println(s.getName());

//            List<Integer> list = new ArrayList<Integer>();
//            int[] ints = {1,2};
//            List<Student>ss = studentDao.findByIds(ints);
//            for (Student s : ss){
//                System.out.println(s.getName());
//            }
            sqlSession.commit();

            sqlSession.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
