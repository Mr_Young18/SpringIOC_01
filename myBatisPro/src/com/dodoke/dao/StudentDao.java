package com.dodoke.dao;

import com.dodoke.bean.Student;

import java.util.List;

public interface StudentDao {

    public List<Student> selectAll();
    public int save(Student s);

    public  Student findByCondition(Student s );

    public  List<Student> findByIds(int[] ints);
    public List<Student> selectAllOne();

}
