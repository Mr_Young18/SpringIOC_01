package com.dodoke.dao;

import com.dodoke.bean.Classes;

import java.util.List;

public interface ClassesDao {

    public List<Classes> selectAll();
}
