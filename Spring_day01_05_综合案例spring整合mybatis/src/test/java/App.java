import com.dodoke.domain.Account;
import com.dodoke.service.AccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService accountService = (AccountService) ctx.getBean("accountService");
        Account account = new Account();
        account.setMoney(50d);

        account.setName("张三");
        accountService.save(account);
    }
}
